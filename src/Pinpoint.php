<?php

declare(strict_types=1);

namespace Drupal\pinpoint;

use Drupal\aws\AwsClientFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Provides methods for the Pinpoint service.
 */
class Pinpoint implements PinpointInterface {

  /**
   * Constructs the Pinpoint service.
   *
   * @param \Drupal\aws\AwsClientFactoryInterface $awsClientFactory
   *   The AWS client factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   */
  public function __construct(
    protected AwsClientFactoryInterface $awsClientFactory,
    protected LoggerChannelInterface $logger,
  ) {}

}
